const axios = require('axios');
const jwt = require("express-jwt");
const jwksRsa = require("jwks-rsa");

var exports=module.exports={};

const authConfig = {
    domain: process.env['AUTH0_DOMAIN'],
    clientId: process.env['AUTH0_CLIENTID'],
    audience: process.env['AUTH0_AUDIENCE'],
}

exports.getAuthConfig = function () {
    return authConfig;
};

// create the JWT middleware
exports.checkJwt = jwt({
    secret: jwksRsa.expressJwtSecret({
      cache: true,
      rateLimit: true,
      jwksRequestsPerMinute: 5,
      jwksUri: `https://${authConfig.domain}/.well-known/jwks.json`
    }),
  
    audience: authConfig.audience,
    issuer: `https://${authConfig.domain}/`,
    algorithms: ["RS256"]
  });

exports.getAccessToken = function (req) {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        console.log(req.headers.authorization.split(' ')[1]);
        return req.headers.authorization.split(' ')[1];
    } else if (req.query && req.query.token) {
        return req.query.token;
    }
    return null;
};

exports.isEmailVerified = function(accessToken, callback) {

    axios.get('https://' + authConfig.domain + '/userinfo',
        {
            headers: {
                Authorization: 'Bearer ' + accessToken
            }
        })
    .then(response => {
        callback(response.data.email_verified);
    })
    .catch(error => {
        console.log(error);
        callback(false);
    });
}