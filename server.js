require('dotenv').config({path: __dirname + '/.env'});
const express = require("express");
const { join } = require("path");
const morgan = require("morgan");
const helmet = require("helmet");
const auth0 = require('./auth0.js');

const app = express();

app.use(morgan("dev"));
app.use(helmet());
app.use(express.static(join(__dirname, "public")));

app.get("/auth_config", (req, res) => {
  res.send(JSON.stringify(auth0.getAuthConfig()));
});

app.get("/", (_, res) => {
  res.sendFile(join(__dirname, "index.html"));
});

app.get("/api/v1/order", auth0.checkJwt, (req, res) => {
  var accessToken = auth0.getAccessToken(req);
  auth0.isEmailVerified(accessToken, function(isEmailVerified) {
    if (isEmailVerified) {
      // Perform the actual order and return the result
      res.send({
        msg: "Your order was placed successfully!"
      });
    } else {
      res.send({
        msg: "Please verify your Email address before placing an order!"
      });
    }
  });

});

app.get("/api/v1/isEmailVerified", auth0.checkJwt, (req, res) => {
  var accessToken = auth0.getAccessToken(req);
  auth0.isEmailVerified(accessToken, function(isEmailVerified) {
    res.send({
      isEmailVerified: isEmailVerified
    });
  });
});

app.use(function(err, req, res, next) {
  if (err.name === "UnauthorizedError") {
    return res.status(401).send({ msg: "Invalid token" });
  }

  next(err, req, res);
});

process.on("SIGINT", function() {
  process.exit();
});

module.exports = app;
